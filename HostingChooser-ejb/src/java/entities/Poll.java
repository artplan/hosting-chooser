
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author artplan
 */
@Entity
@Table(name = "poll")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Poll.findAll", query = "SELECT p FROM Poll p"),
    @NamedQuery(name = "Poll.findByIdpoll", query = "SELECT p FROM Poll p WHERE p.idpoll = :idpoll"),
    @NamedQuery(name = "Poll.findByName", query = "SELECT p FROM Poll p WHERE p.name = :name"),
    @NamedQuery(name = "Poll.findByHit", query = "SELECT p FROM Poll p WHERE p.hit = :hit")})
public class Poll implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpoll")
    private Integer idpoll;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    @Column(name = "hit")
    private Integer hit;

    public Poll() {
    }

    public Poll(Integer idpoll) {
        this.idpoll = idpoll;
    }

    public Integer getIdpoll() {
        return idpoll;
    }

    public void setIdpoll(Integer idpoll) {
        this.idpoll = idpoll;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpoll != null ? idpoll.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Poll)) {
            return false;
        }
        Poll other = (Poll) object;
        if ((this.idpoll == null && other.idpoll != null) || (this.idpoll != null && !this.idpoll.equals(other.idpoll))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Poll[ idpoll=" + idpoll + " ]";
    }
    
}
