/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author artplan
 */
@Entity
@Table(name = "groups")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserGroup.findAll", query = "SELECT u FROM UserGroup u"),
    @NamedQuery(name = "UserGroup.findByGroupname", query = "SELECT u FROM UserGroup u WHERE u.groupname = :groupname"),
    @NamedQuery(name = "UserGroup.findByUser", query = "SELECT u FROM UserGroup u WHERE u.user = :user"),
    @NamedQuery(name = "UserGroup.findByIdgroup", query = "SELECT u FROM UserGroup u WHERE u.idgroup = :idgroup")})
public class UserGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "groupname")
    private String groupname;
    @Size(max = 45)
    @Column(name = "user")
    private String user;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgroup")
    private Integer idgroup;

    public UserGroup() {
    }

    public UserGroup(Integer idgroup) {
        this.idgroup = idgroup;
    }

    public UserGroup(Integer idgroup, String groupname) {
        this.idgroup = idgroup;
        this.groupname = groupname;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getIdgroup() {
        return idgroup;
    }

    public void setIdgroup(Integer idgroup) {
        this.idgroup = idgroup;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgroup != null ? idgroup.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserGroup)) {
            return false;
        }
        UserGroup other = (UserGroup) object;
        if ((this.idgroup == null && other.idgroup != null) || (this.idgroup != null && !this.idgroup.equals(other.idgroup))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserGroup[ idgroup=" + idgroup + " ]";
    }
    
}
