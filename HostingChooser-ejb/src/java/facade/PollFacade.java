package facade;

import entities.Poll;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class PollFacade extends AbstractFacade<Poll> implements PollFacadeLocal {
    @PersistenceContext(unitName = "HostingChooser-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PollFacade() {
        super(Poll.class);
    }
    
}
