
package facade;

import entities.Poll;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author artplan
 */
@Local
public interface PollFacadeLocal {

    void create(Poll poll);

    void edit(Poll poll);

    void remove(Poll poll);

    Poll find(Object id);

    List<Poll> findAll();

    List<Poll> findRange(int[] range);

    int count();
    
}
