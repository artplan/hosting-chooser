/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import entities.Post;
import entities.User;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author artplan
 */
@Stateless
public class PostFacade extends AbstractFacade<Post> implements PostFacadeLocal {
    @PersistenceContext(unitName = "HostingChooser-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PostFacade() {
        super(Post.class);
    }

    @Override
    public Post getRand() {
        int c = count();
        if(c!=0){
            Random rnd = new Random();
            Query q = em.createQuery("SELECT p FROM Post p");
            q.setFirstResult(rnd.nextInt(c));
            q.setMaxResults(1);
            return (Post) q.getSingleResult();
        }
        else{
            return null;
        }
    }

    @Override
    public List<Post> findByAuthor(User user) {
        Query q = em.createNamedQuery("Post.findByAuthor");
        q.setParameter("author", user);
        return q.getResultList();
    }
}