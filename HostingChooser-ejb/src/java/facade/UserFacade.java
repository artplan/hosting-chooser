/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import entities.User;
import entities.UserGroup;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author artplan
 */
@Stateless
public class UserFacade extends AbstractFacade<User> implements UserFacadeLocal {
    @EJB
    private UserGroupFacadeLocal userGroupFacade;
    @PersistenceContext(unitName = "HostingChooser-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserFacade() {
        super(User.class);
    }

    @Override
    public User findByUsername(String username) {
        Query q = em.createNamedQuery("User.findByUsername");
        q.setParameter("username", username);
        return (User) q.getSingleResult();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void add(User user) {
        if(user.getIduser()==null){
           user.setPassword(md5(user.getPassword()));
           UserGroup ug = new UserGroup();
           ug.setGroupname("User");
           ug.setUser(user.getUsername());
           userGroupFacade.create(ug);
        }
        else{
            User u = find(user.getIduser());
            if(!u.getUsername().equals(user.getUsername())){
               List<UserGroup> ugs = userGroupFacade.findByUserName(u.getUsername());
               for(UserGroup ug : ugs){
                   ug.setUser(user.getUsername());
                   userGroupFacade.edit(ug);
               }
            }
            if(!u.getPassword().equals(user.getPassword())){
               user.setPassword(md5(user.getPassword()));
            }
        }
        edit(user);
    }

    @Override
    public void delete(User user) {
        List<UserGroup> ugs = userGroupFacade.findByUserName(user.getUsername());
        for(UserGroup ug : ugs){
            userGroupFacade.remove(ug);
        }
        remove(user);
    }
    
    public static String md5(String mes) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(mes.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            return hash.toString(16);
        } catch (NoSuchAlgorithmException e) {
            return "Error";
        }   
    }
}
