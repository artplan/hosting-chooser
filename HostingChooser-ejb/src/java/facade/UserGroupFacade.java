/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import entities.UserGroup;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author artplan
 */
@Stateless
public class UserGroupFacade extends AbstractFacade<UserGroup> implements UserGroupFacadeLocal {
    @PersistenceContext(unitName = "HostingChooser-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserGroupFacade() {
        super(UserGroup.class);
    }

    @Override
    public List<UserGroup> findByUserName(String username) {
        Query q = em.createNamedQuery("UserGroup.findByUser");
        q.setParameter("user", username);
        return q.getResultList();
    }
    
}
