/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import entities.UserGroup;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author artplan
 */
@Local
public interface UserGroupFacadeLocal {

    void create(UserGroup userGroup);

    void edit(UserGroup userGroup);

    void remove(UserGroup userGroup);

    UserGroup find(Object id);
    
    List<UserGroup> findByUserName(String username);

    List<UserGroup> findAll();

    List<UserGroup> findRange(int[] range);

    int count();
    
}
