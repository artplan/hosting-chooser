package facade;

import entities.Shared;
import entities.User;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

@Stateful(name="USF")
@StatefulTimeout(unit = TimeUnit.MINUTES, value = 15)
public class UserSessionFacade implements UserSessionFacadeLocal {
    @EJB
    private UserFacadeLocal userFacade;
    private List<Shared> favouriteShared;

    private User user;
    
    @PostConstruct
    private void init(){
        favouriteShared = new LinkedList<>();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(String username) {
        this.user = userFacade.findByUsername(username);
    }

    @Override
    public List<Shared> getFavouriteShared() {
        return favouriteShared;
    }

    public void setFavouriteShared(List<Shared> favouriteShared) {
        this.favouriteShared = favouriteShared;
    }

    @Override
    public void addInFavourites(Shared s) {
        favouriteShared.add(s);
    }

    @Override
    public void delFromFav(Shared s) {
        favouriteShared.remove(s);
    }
}
