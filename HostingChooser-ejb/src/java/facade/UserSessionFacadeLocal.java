
package facade;

import entities.Shared;
import entities.User;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author artplan
 */
@Local
public interface UserSessionFacadeLocal {

    public User getUser();

    public void setUser(String username);
    
    public List<Shared> getFavouriteShared();
    
    public void addInFavourites(Shared s);
    
    public void delFromFav(Shared s);
    
}
