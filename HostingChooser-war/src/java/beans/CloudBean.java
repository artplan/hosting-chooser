package beans;

import entities.Cloud;
import facade.CloudFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

@Named(value = "cloudBean")
@ViewScoped
public class CloudBean implements Serializable{
    @EJB
    private CloudFacadeLocal cloudFacade;
    private Cloud cloud;
    private List<Cloud> cloudList;

    public CloudBean() {
        cloud=new Cloud();
    }

    @PostConstruct
    protected void postConstruct(){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if(!request.getParameterMap().isEmpty()){
            if(request.getParameter("price")!=null){cloud.setPrice(Integer.parseInt(request.getParameter("price")));}
            if(request.getParameter("space")!=null){cloud.setSpace(Integer.parseInt(request.getParameter("space")));}
            if(request.getParameter("traffic")!=null){cloud.setTraffic(Integer.parseInt(request.getParameter("traffic")));}
            setCloudList(cloudFacade.search(cloud,true));
        }
    }
    
    public void clear(){
        cloud = new Cloud();
    }
    
    public void search(){
        setCloudList(null);
        if(cloud.getSitemove()==false){cloud.setSitemove(null);}
        setCloudList(cloudFacade.search(cloud,false));
    }
    
    public String addCloud(){
        cloudFacade.edit(cloud);
        return "cloud?faces-redirect=true";
    }
    
    public String delCloud(Cloud cloud){
        cloudFacade.remove(cloud);
        return "cloud?faces-redirect=true";
    }
    
    public Cloud getCloud() {
        return cloud;
    }

    public void setCloud(Cloud cloud) {
        this.cloud = cloud;
    }

    public List<Cloud> getCloudList() {
        return cloudList;
    }
    
    public void setCloudList(List<Cloud> cloudList) {
        this.cloudList = cloudList;
    }
}