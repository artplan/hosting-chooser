package beans;

import entities.Dedicated;
import facade.DedicatedFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

@Named(value = "dedicBean")
@ViewScoped
public class DedicBean implements Serializable{
    @EJB
    private DedicatedFacadeLocal dedicatedFacade;
    private List<Dedicated> dedicList;
    private Dedicated dedic;

    
    public DedicBean() {
        dedic = new Dedicated();
    }
    
    @PostConstruct
    protected void postConstruct(){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if(!request.getParameterMap().isEmpty()){
            if(request.getParameter("price")!=null){dedic.setPrice(Integer.parseInt(request.getParameter("price")));}
            if(request.getParameter("space")!=null){dedic.setSpace(Integer.parseInt(request.getParameter("space")));}
            if(request.getParameter("traffic")!=null){dedic.setTraffic(Integer.parseInt(request.getParameter("traffic")));}
            dedicList = dedicatedFacade.search(dedic,true);
        }
    }
    
    public void clear(){
        dedic = new Dedicated();
    }
    
    public void search(){
        dedicList = null;
        dedicList = dedicatedFacade.search(dedic,false);
    }
        
    public String addDedic(){
        dedicatedFacade.edit(getDedic());
        return "dedicated?faces-redirect=true";
    }
    
    public String delDedic(Dedicated dedic){
        dedicatedFacade.remove(dedic);
        return "dedicated?faces-redirect=true";
    }

    public List<Dedicated> getDedicList() {
        return dedicList;
    }

    public void setDedicList(List<Dedicated> dediclist) {
        this.dedicList = dediclist;
    }

    public Dedicated getDedic() {
        return dedic;
    }

    public void setDedic(Dedicated dedic) {
        this.dedic = dedic;
    }
}