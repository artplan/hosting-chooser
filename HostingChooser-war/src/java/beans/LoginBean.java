package beans;

import facade.UserSessionFacadeLocal;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named
@RequestScoped
public class LoginBean implements Serializable{
    private String username;
    private String password;
    public LoginBean() {
    }
    
    public void login(){
        login(username,password);
    }
    
    public void login (String username, String password){
    FacesContext fc = FacesContext.getCurrentInstance();
    HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        try{
            request.logout();
            request.login(username, password);
            try {
                UserSessionFacadeLocal usf = (UserSessionFacadeLocal) request.getSession().getAttribute("userSessionFacadeLocal");
                if(usf==null){
                    InitialContext ic = new InitialContext();
                    usf = (UserSessionFacadeLocal) ic.lookup("java:global/HostingChooser/HostingChooser-ejb/USF!facade.UserSessionFacadeLocal");
                    request.getSession().setAttribute("userSessionFacadeLocal", usf);
                }
                usf.setUser(username);
            }catch (NamingException e) {
                System.out.println(e.getMessage());
            }
        }
        catch (ServletException e) {
            fc.addMessage(null, new FacesMessage("Вход не выполнен"));
            System.out.println(e.getMessage());
        }
    }

    public String logout() {
    HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    request.getSession().removeAttribute("userSessionFacadeLocal");
    try {
      request.logout();
    } catch (ServletException e) {
        System.out.println(e.getMessage());
    }
    return "index?faces-redirect=true";
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}