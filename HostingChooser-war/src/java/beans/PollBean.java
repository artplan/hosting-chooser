package beans;

import entities.Poll;
import facade.PollFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

@Named(value = "pollBean")
@ViewScoped
public class PollBean {
    @EJB
    private PollFacadeLocal pollFacade;
    private int pollvar;
    private final String[] colors;
    private List<Poll> polls;

    public PollBean() {
        this.colors = new String[]{"progress-bar-info", "progress-bar-success", "progress-bar-warning", "progress-bar-danger",""};
    }
    
    @PostConstruct
    protected void postConstructor(){
        getAll();
    }
    
    public void getAll(){
        setPolls(pollFacade.findAll());
    }
       
    public void setHit(){
        pollvar = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param"));
    }
    
    public void hit(){
        Poll p = pollFacade.find(pollvar);
        p.setHit(p.getHit()+1);
        pollFacade.edit(p);
        getAll();
    }

    public int getPollvar() {
        return pollvar;
    }

    public void setPollvar(int pollvar) {
        this.pollvar = pollvar;
    }

    public String[] getColors() {
        return colors;
    }

    /**
     * @return the polls
     */
    public List<Poll> getPolls() {
        return polls;
    }

    /**
     * @param polls the polls to set
     */
    public void setPolls(List<Poll> polls) {
        this.polls = polls;
    }
}