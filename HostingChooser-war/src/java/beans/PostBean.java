package beans;

import entities.Post;
import entities.User;
import facade.PostFacadeLocal;
import facade.UserSessionFacadeLocal;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.event.FileUploadEvent;

@Named(value = "postBean")
@ViewScoped
public class PostBean implements Serializable{
    @EJB
    private PostFacadeLocal postFacade;
    private Post post;
    private List<Post> posts;
    
    public PostBean() {
        post = new Post();
    }
    
    @PostConstruct
    protected void postConstructor(){
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String path = req.getServletPath();
        if("/blog.xhtml".equals(path)){
            getPostList();
        }
        if("/profile.xhtml".equals(path)){
           UserSessionFacadeLocal usf = (UserSessionFacadeLocal) req.getSession().getAttribute("userSessionFacadeLocal");
           posts = req.isUserInRole("Admin") ? postFacade.findAll() : postFacade.findByAuthor(usf.getUser());
        }
    }
    
    public void clear(){
        post = new Post();
    }
    
    public void getPostList(){
        setPosts(postFacade.findAll());
    }
    
    public void handleFileUpload(FileUploadEvent event) throws IOException{
        InputStream inputstream = event.getFile().getInputstream();
        String s = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/img")+"\\"+event.getFile().getFileName();
        try (FileOutputStream fos = new FileOutputStream(new File("/../"+event.getFile().getFileName()))){
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputstream.read(bytes)) != -1) {
                fos.write(bytes, 0, read);
            }
            System.out.println(s);
            post.setImg("");
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    public Post getRnd(){
        return postFacade.getRand();
    }
    
    public String addPost(){
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        UserSessionFacadeLocal usf = (UserSessionFacadeLocal) req.getSession().getAttribute("userSessionFacadeLocal");
        java.util.Date dt = new java.util.Date();
        post.setTime(dt);
        post.setAuthor(usf.getUser());
        postFacade.edit(getPost());
        return "profile?faces-redirect=true";
    }
    
    public String delPost(Post post){
        postFacade.remove(post);
        return "profile?faces-redirect=true";
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    /**
     * @return the posts
     */
    public List<Post> getPosts() {
        return posts;
    }

    /**
     * @param posts the posts to set
     */
    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}