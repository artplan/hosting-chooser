
package beans;

import entities.User;
import entities.UserGroup;
import facade.UserFacadeLocal;
import facade.UserGroupFacadeLocal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author artplan
 */
@Named(value = "registrationBean")
@RequestScoped
public class RegistrationBean {
    @EJB
    private UserGroupFacadeLocal userGroupFacade;
    @EJB
    private UserFacadeLocal userFacade;
    @Inject
    private LoginBean loginBean;
    private User user;
    
    public RegistrationBean() {
        user=new User();
    }
    
    @PostConstruct
    protected void postConstructor(){
        
    }
    
    public String register() throws ServletException{
        String pass = user.getPassword();
        getUser().setPassword(md5(getUser().getPassword()));
        UserGroup ug = new UserGroup();
        ug.setGroupname("User");
        ug.setUser(getUser().getUsername());
        userGroupFacade.create(ug);
        
        userFacade.create(getUser());
        
        loginBean.login(user.getUsername(), pass);
        return "profile?faces-redirect=true";
    }
    
    public static String md5(String mes) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(mes.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            return hash.toString(16);
        } catch (NoSuchAlgorithmException e) {
            return "Error";
        }   
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }
}
