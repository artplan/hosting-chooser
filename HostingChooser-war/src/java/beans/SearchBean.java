package beans;

import entities.Backup;
import entities.Cpu;
import entities.Hdd;
import entities.Panel;
import entities.Platform;
import entities.Support;
import entities.Visualisation;
import facade.BackupFacadeLocal;
import facade.CpuFacadeLocal;
import facade.HddFacadeLocal;
import facade.PanelFacadeLocal;
import facade.PlatformFacadeLocal;
import facade.SupportFacadeLocal;
import facade.VisualisationFacadeLocal;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;

@Named(value = "searchBean")
@ViewScoped
public class SearchBean {
    @EJB
    private VisualisationFacadeLocal visualisationFacade;
    @EJB
    private BackupFacadeLocal backupFacade;
    @EJB
    private PlatformFacadeLocal platformFacade;
    @EJB
    private PanelFacadeLocal panelsFacade;
    @EJB
    private SupportFacadeLocal supportFacade;
    @EJB
    private CpuFacadeLocal cpuFacade;
    @EJB
    private HddFacadeLocal hddFacade;
    
    public SearchBean() {}
    
    public List<Hdd> getHddList(){
        return hddFacade.findAll();
    }
    
    public List<Cpu> getCpuList(){
        return cpuFacade.findAll();
    }
    
    public List<Support> getSupportList(){
        return supportFacade.findAll();
    }
    
    public List<Panel> getPanelList(){
        return panelsFacade.findAll();
    }
    
    public List<Platform> getPlatformList(){
        return platformFacade.findAll();
    }
    
    public List<Backup> getBackupList(){
        return backupFacade.findAll();
    }
    
    public List<Visualisation> getVisualisationList(){
        return visualisationFacade.findAll();
    }
}