/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import entities.Shared;
import facade.SharedFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author artplan
 */
@Named(value = "sharedBean")
@ViewScoped
public class SharedBean implements Serializable{
    @EJB
    private SharedFacadeLocal sharedFacade;
    private List<Shared> sharedList;
    private Shared shared = new Shared();

    public SharedBean() {
    }
    
    @PostConstruct
    protected void postConstruct(){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if(!request.getParameterMap().isEmpty())
        {
            if(request.getParameter("price")!=null){shared.setPrice(Integer.parseInt(request.getParameter("price")));}
            if(request.getParameter("space")!=null){shared.setSpace(Integer.parseInt(request.getParameter("space")));}
            if(request.getParameter("traffic")!=null){shared.setTraffic(Integer.parseInt(request.getParameter("traffic")));}
            sharedList = sharedFacade.search(shared,true);
        }
            
    }
    
    public void clear(){
        shared = new Shared();
    }
    
    public void search(){
        sharedList = null;
        if(shared.getAsp()==false){shared.setAsp(null);}
        if(shared.getPhp()==false){shared.setPhp(null);}
        if(shared.getSsh()==false){shared.setSsh(null);}
        sharedList = sharedFacade.search(shared,false);
    }
    
    public String addShared(){
        sharedFacade.edit(shared);
        return "shared?faces-redirect=true";
    }
    
    public String delShared(Shared shared){
        sharedFacade.remove(shared);
        return "shared?faces-redirect=true";
    }

    /**
     * @return the sharedList
     */
    public List<Shared> getSharedList() {
        return sharedList;
    }

    /**
     * @param sharedList the sharedList to set
     */
    public void setSharedList(List<Shared> sharedList) {
        this.sharedList = sharedList;
    }

    /**
     * @return the shared
     */
    public Shared getShared() {
        return shared;
    }

    /**
     * @param shared the shared to set
     */
    public void setShared(Shared shared) {
        this.shared = shared;
    }
    
}
