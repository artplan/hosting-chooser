package beans;

import entities.User;
import facade.UserFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class UserBean implements Serializable{
    @EJB
    private UserFacadeLocal userFacade;
    
    private User user;

    public UserBean() {
        user = new User();
    }
    
    public void clear(){
        user = new User();
    }
    
    public String addUser()
    {
        userFacade.add(user);
        return "profile?faces-redirect=true";
    }
            
    public String delUser(User usr)
    {
        userFacade.delete(usr);
        return "profile?faces-redirect=true";
    }
    
    public List<User> getUsrList() {
        return userFacade.findAll();
    }
    
    public User getUserById(int id){
        return userFacade.find(id);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }   
}