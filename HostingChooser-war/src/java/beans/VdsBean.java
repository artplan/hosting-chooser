package beans;

import entities.Vds;
import facade.VdsFacadeLocal;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

@Named(value = "vdsBean")
@ViewScoped
public class VdsBean implements Serializable{
    @EJB
    private VdsFacadeLocal vdsFacade;
    private Vds vds;
    private List<Vds> vdsList;
    
    public VdsBean() {
        vds=new Vds();
    }
    
    @PostConstruct
    protected void postConstruct(){
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if(!request.getParameterMap().isEmpty()){
            if(request.getParameter("price")!=null){vds.setPrice(Integer.parseInt(request.getParameter("price")));}
            if(request.getParameter("space")!=null){vds.setSpace(Integer.parseInt(request.getParameter("space")));}
            if(request.getParameter("traffic")!=null){vds.setTraffic(Integer.parseInt(request.getParameter("traffic")));}
            setVdsList(vdsFacade.search(vds,true));
        }
    }
    
    public void clear(){
        vds = new Vds();
    }
    
    public void search(){
        setVdsList(null);
        if(vds.getSitemove()==false){vds.setSitemove(null);}
        if(vds.getDns()==false){vds.setDns(null);}
        setVdsList(vdsFacade.search(vds,false));
    }
    
    public String addVds(){
        vdsFacade.edit(vds);
        return "vds?faces-redirect=true";
    }
    
    public String delVds(Vds vds){
        vdsFacade.remove(vds);
        return "vds?faces-redirect=true";
    }

    public Vds getVds() {
        return vds;
    }

    public void setVds(Vds vds) {
        this.vds = vds;
    }

    public List<Vds> getVdsList() {
        return vdsList;
    }

    public void setVdsList(List<Vds> vdsList) {
        this.vdsList = vdsList;
    }
    
}