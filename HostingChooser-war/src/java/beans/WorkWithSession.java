package beans;

import entities.Shared;
import facade.UserSessionFacadeLocal;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.servlet.http.HttpServletRequest;

@Named(value = "workWithSession")
@ViewScoped
public class WorkWithSession {
    private HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    private UserSessionFacadeLocal usf = (UserSessionFacadeLocal) request.getSession().getAttribute("userSessionFacadeLocal");

    public WorkWithSession() {

    }
    
    public void addInFavourites(Shared s){
        usf.addInFavourites(s);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Добавлено в избранное", s.getName()));
    }
    
    public void deleteFromFav(Shared s){
        usf.delFromFav(s);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Удалено из избранного", s.getName()));
    }
    
    public List<Shared> getFavourites(){
        return usf.getFavouriteShared();
    }
}