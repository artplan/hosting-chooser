/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Backup;
import facade.BackupFacadeLocal;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named(value = "backupConveter")
@RequestScoped
@FacesConverter(value = "backupConverter")
public class BackupConverter implements Converter{
    @EJB
    private BackupFacadeLocal backupFacade;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Backup) backupFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Backup){
        return ((Backup) value).getIdbackup().toString();
        }
        return "";
    }
    
}
