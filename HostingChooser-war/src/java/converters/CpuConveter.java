/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Cpu;
import facade.CpuFacadeLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author artplan
 */
@Named(value = "cpuConveter")
@RequestScoped
@FacesConverter(value = "cpuConverter")
public class CpuConveter implements Converter{
    @EJB
    private CpuFacadeLocal cpuFacade;

    /**
     * Creates a new instance of CpuConveter
     */
    public CpuConveter() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Cpu) cpuFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Cpu){
        return ((Cpu) value).getIdcpu().toString();
        }
        return "";
    }
    
}
