/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Cpu;
import entities.Hdd;
import facade.CpuFacadeLocal;
import facade.HddFacadeLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author artplan
 */
@Named(value = "hddConveter")
@RequestScoped
@FacesConverter(value = "hddConverter")
public class HddConverter implements Converter{
    @EJB
    private HddFacadeLocal hddFacade;

    /**
     * Creates a new instance of CpuConveter
     */
    public HddConverter() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Hdd) hddFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Hdd){
            return ((Hdd) value).getIdhdd().toString();
        }
        return "";
    }
    
}
