/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Panel;
import facade.PanelFacadeLocal;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named(value = "panelConveter")
@RequestScoped
@FacesConverter(value = "panelConverter")
public class PanelConverter implements Converter{
    @EJB
    private PanelFacadeLocal panelFacade;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Panel) panelFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Panel){
        return ((Panel) value).getIdpanel().toString();
        }
        return "";
    }
    
}
