/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Platform;
import facade.PlatformFacadeLocal;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

/**
 *
 * @author artplan
 */
@Named(value = "platformConveter")
@RequestScoped
@FacesConverter(value = "platformConverter")
public class PlatformConverter implements Converter{
    @EJB
    private PlatformFacadeLocal platformFacade;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Platform) platformFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Platform){
        return ((Platform) value).getIdplatform().toString();
        }
        return "";
    }
    
}
