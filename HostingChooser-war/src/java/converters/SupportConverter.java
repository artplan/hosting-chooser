/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package converters;

import entities.Cpu;
import entities.Support;
import facade.CpuFacadeLocal;
import facade.SupportFacadeLocal;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author artplan
 */
@Named(value = "supportConveter")
@RequestScoped
@FacesConverter(value = "supportConverter")
public class SupportConverter implements Converter{
    @EJB
    private SupportFacadeLocal supportFacade;

    /**
     * Creates a new instance of CpuConveter
     */
    public SupportConverter() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Support) supportFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Support){
        return ((Support) value).getIdsupport().toString();
        }
        return "";
    }
    
}
