package converters;

import entities.Visualisation;
import facade.VisualisationFacadeLocal;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named(value = "visualisationConveter")
@RequestScoped
@FacesConverter(value = "visualisationConverter")
public class VisualisationConverter implements Converter{
    @EJB
    private VisualisationFacadeLocal visualisationFacade;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return (Visualisation) visualisationFacade.find(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if(value!=null && value instanceof Visualisation){
        return ((Visualisation) value).getIdvisualisation().toString();
        }
        return "";
    }
}